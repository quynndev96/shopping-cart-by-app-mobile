package com.tsolution.shoppingcart.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Product implements Serializable {

    @SerializedName("ID")
    private int ID;
    @SerializedName("Name")
    private String Name;
    @SerializedName("Price")
    private double Price;
    @SerializedName("CreateDate")
    private Date CreateDate;
    @SerializedName("Status")
    private Enum Status;

    public Product(int ID, String name, double price, Date createDate, Enum status) {
        this.ID = ID;
        Name = name;
        Price = price;
        CreateDate = createDate;
        Status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date createDate) {
        CreateDate = createDate;
    }

    public Enum getStatus() {
        return Status;
    }

    public void setStatus(Enum status) {
        Status = status;
    }

    enum Status{
        Yes, No
    }
}
